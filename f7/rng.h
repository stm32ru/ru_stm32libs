#ifndef UTIL_RNG_H_
#define UTIL_RNG_H_
#include "stm32f7xx.h"

void rng_init(void);
uint32_t rng_getRandom(void);


#endif /* UTIL_RNG_H_ */
