#ifndef _RU_STM32_RCC_H_
#define _RU_STM32_RCC_H_
#include "stm32f7xx.h"

static inline void rcc_AHB1ENR_set(uint32_t b) {
	RCC->AHB1ENR |= b;
	__DSB();
}

static inline void rcc_AHB1ENR_clear(uint32_t b) {
	RCC->AHB1ENR &= ~b;
	__DSB();
}

static inline void rcc_AHB2ENR_set(uint32_t b) {
	RCC->AHB2ENR |= b;
	__DSB();
}

static inline void rcc_AHB2ENR_clear(uint32_t b) {
	RCC->AHB2ENR &= ~b;
	__DSB();
}

static inline void rcc_AHB3ENR_set(uint32_t b) {
	RCC->AHB3ENR |= b;
	__DSB();
}

static inline void rcc_AHB3ENR_clear(uint32_t b) {
	RCC->AHB3ENR &= ~b;
	__DSB();
}

static inline void rcc_APB1ENR_set(uint32_t b) {
	RCC->APB1ENR |= b;
	__DSB();
}

static inline void rcc_APB1ENR_clear(uint32_t b) {
	RCC->APB1ENR &= ~b;
	__DSB();
}

static inline void rcc_APB2ENR_set(uint32_t b) {
	RCC->APB2ENR |= b;
	__DSB();
}

static inline void rcc_APB2ENR_clear(uint32_t b) {
	RCC->APB2ENR &= ~b;
}

void rcc_usePLL216Mhz(void);

#endif /* !_RU_STM32_RCC_H_ */
