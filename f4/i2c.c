#include "stm32f4xx.h"
#include "i2c.h"
#include "stdbool.h"

//////for i2c 100kHz ond cpu clock 8MHz
////#define SCLL 	0x13
////#define SCLH	0xF
////#define SDADEL	0x2
////#define SCLDEL	0x4
////#define PRESC	1
//
////for i2c 400kHz ond cpu clock 8MHz
//#define SCLL 	0x09
//#define SCLH	0x03
//#define SDADEL	0x01
//#define SCLDEL	0x03
//#define PRESC	0

static i2cInstance_t *__I2C1, *__I2C2, *__I2C3;
static uint8_t index = 0;

void i2c_init(i2cInstance_t *i2cInstance) {
	i2cInstance->I2Cx->CR1 &= ~I2C_CR1_PE;

	i2cInstance->I2Cx->CR2 |= I2C_CR2_ITEVTEN | I2C_CR2_ITBUFEN;
//	i2cInstance->I2Cx->CR1 |= I2C_CR1_ACK;		//Wlaczenie potwierdzen
	i2cInstance->I2Cx->CR2 |= 42; 				//ustawienie APB1 na 42MHz
	i2cInstance->I2Cx->CCR |= 210; 			//SCL 100kHz -> 100kHz = 42MHz/CCR

	i2cInstance->I2Cx->TRISE = 0x0002; //resetowanie rejestru
	i2cInstance->I2Cx->TRISE |= 43; //minimalny czas narastania 1000ns -> TRISE-1=1000ns*42MHz

//	i2cInstance->I2Cx->TIMINGR = SCLL | (SCLH << 8) | (SDADEL << 16) | (SCLDEL << 20) | (PRESC << 28);

	i2cInstance->I2Cx->CR1 |= I2C_CR1_PE;

	if (i2cInstance->I2Cx == I2C1) {
		__I2C1 = i2cInstance;
		NVIC_EnableIRQ(I2C1_EV_IRQn);
	} else if (i2cInstance->I2Cx == I2C2) {
		__I2C2 = i2cInstance;
		NVIC_EnableIRQ(I2C2_EV_IRQn);
	} else if (i2cInstance->I2Cx == I2C3) {
		__I2C3 = i2cInstance;
		NVIC_EnableIRQ(I2C3_EV_IRQn);
	}
}

bool i2c_sendData(i2cInstance_t *i2cInstance) {

	while (i2cInstance->I2Cx->CR1 & I2C_CR1_STOP)
		;
	index = 0;
	i2cInstance->busBusy = true;
	i2cInstance->I2Cx->CR1 |= I2C_CR1_START;

	return true;
}

static void i2c_interruptCallback(i2cInstance_t *i2cInstance) {

	if (i2cInstance->I2Cx->SR1 & I2C_SR1_SB) {
		(void) i2cInstance->I2Cx->SR2;
		i2cInstance->I2Cx->DR = i2cInstance->slaveAddress;
	} else if (i2cInstance->I2Cx->SR1 & I2C_SR1_ADDR) {
		(void) i2cInstance->I2Cx->SR2;
	} else if ((i2cInstance->I2Cx->SR1 & I2C_SR1_TXE)
			|| (i2cInstance->I2Cx->SR1 & I2C_SR1_BTF)) {

		if (index < i2cInstance->dataSize) {
			i2cInstance->I2Cx->DR = i2cInstance->dataBuffer[index++];
		} else {
			i2cInstance->I2Cx->CR1 |= I2C_CR1_STOP;
			index = 0;
			i2cInstance->busBusy = false;
//			for(int i=500; i; --i);
//			while (i2cInstance->I2Cx->CR1 & I2C_CR1_STOP);
		}
	}

}

void I2C1_EV_IRQHandler(void) {
	i2c_interruptCallback(__I2C1);
}

void I2C2_EV_IRQHandler(void) {
	i2c_interruptCallback(__I2C2);
}

void I2C3_EV_IRQHandler(void) {
	i2c_interruptCallback(__I2C3);
}
